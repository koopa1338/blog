---
title: "About"
date: 2019-02-22T01:25:17+01:00
draft: false
---

Hi und willkommen auf meinem Blog,

ich schreibe hier über verschieden Dinge
wie zum Beispiel Sport, Ernährung, aber
auch über Sachen aus der IT wie Netzpolitik, Tutorials, Hardware etc. und gelegentlich
auch über Projekte aus meinem täglichen Leben.

Falls ihr Anregungen oder Ideen habt,
dann schreibt mir eine Mail oder bei Mastodon. Viel Spass beim lesen :wink:

## Haftungsausschluss:

### Haftung für Links

Meine Inhalte können Links zu externen Webseiten Dritter enthalten, auf deren
Inhalte ich keinen Einfluss habe. Deshalb kann ich für diese fremden Inhalte
auch keine Gewähr übernehmen. Für die Inhalte der verlinkten Seiten ist stets
der jeweilige Anbieter oder Betreiber der Seiten verantwortlich. Die verlinkten
Seiten wurden zum Zeitpunkt der Verlinkung auf mögliche Rechtsverstöße
überprüft. Rechtswidrige Inhalte waren zum Zeitpunkt der Verlinkung nicht
erkennbar. Eine permanente inhaltliche Kontrolle der verlinkten Seiten ist
jedoch ohne konkrete Anhaltspunkte einer Rechtsverletzung nicht zumutbar. Bei
Bekanntwerden von Rechtsverletzungen werde ich derartige Links umgehend
entfernen.

## Datenschutzerklärung

Es werden keine Daten erhoben, gespeichert oder ausgewertet.
