---
title: "Static Site Generator"
date: 2019-05-19T15:31:10+02:00
draft: false
toc: true
featuredImg: /media/akio_bako.jpg
tags:
    - Code
    - Hugo
---

## Hugo aufsetzen

Die allgemeine [Dokumentation](https://gohugo.io/documentation/) von Hugo ist sehr gut und sollte ein
guter Einstiegspunkt sein, sofern man des englischen mächtig ist. Die
Installation von Hugo ist dort auch beschrieben.

Da ich Linux-Nutzer bin, beschreibe ich hier nur die Vorgehensweise unter Linux,
wenn ihr Windows nutzt, müsst ihr gegebenenfalls einige Dinge anders machen.

Zu aller erst öffnet ihr mal euer Terminal. Standardmäßig solltet ihr dann in eurem
Heimverzeichnis landen. mit `hugo new site Seite` legt ihr eine neue Seite an, die sich dann im
Ordner `Seite` befindet. Ihr könnt das natürlich nennen wie ihr wollt.

Toll, wir haben eine neue Seite, aber wir müssen Hugo jetzt auch ein Theme geben sonst
weiß Hugo ja nicht wie die Seite aussehen soll. Dazu schaut euch mal im Internet um,
es gibt haufenweise Themes. Die liegen zum größten Teil auf `github` also werden wir im nächsten Schritt
erst mal eine ordentliche Versionierung von unserem Projekt machen.

## Klonst du noch oder commitest du schon?

Also wir haben jetzt unseren Ordner für die Seite, mit einem `cd Seite` wechseln wir in den Ordner.
`git` sollte bei jeder Distribution vorinstalliert sein, prüfen können wir das ganz schnell indem wir
`git --version` eingeben.

Jetzt machen wir aus unserem Projekt ein _richtiges_ Projekt: `git init` macht aus unserem aktuellen Ordner
ein Projekt mit Versionskontrolle. Wenn ihr noch nicht genau wisst was `git` eigentlich ist, dann schaut mal
auf deren [Webseite](https://git-scm.com/) vorbei. Jetzt machen wir unseren ersten `commit`, was nichts anderes bedeutet, dass
wir alle Änderung wie eine Art Speicherstand festlegen. Hierzu `git commit -a -m 'Mein erster Commit'` der `-m`
Schalter ist für eine Commitmessage, ihr dürft gerne schreiben was ihr möchtet, aber bei
zukünftigen Commits solltet ihr dort grob rein schreiben was ihr gemacht hab.

## Theme

Habt ihr nun euer Theme gefunden solltet ihr der Anleitung zur installation folgen. Wenn es als Repository
auf Github liegt solltet ihr es als Submodule klonen. Das steht meistens auch in der README vom Theme wie das geht,
haltet einfach ausschau nach etwas mit `git submodule add`.

In jedem Theme ist eine `examplesite` wo ihr eine `config.toml` findet. Die solltet ihr in euren
Hauptordner vom Projekt kopieren, die bereits vorhandene könnt ihr einfach überschreiben.
Diese config sollte zumindest grob das Theme konfigurieren. Meistens stehen auch
noch Kommentare in der Datei, was welche Einstellung bewirkt, ansonsten einfach mal auf der Githubseite vom
Theme nachlesen.

## Content

Ok, jetzt brauchen wir ja auch irgendwas, was unsere Seite anzeigt, dazu generieren wir einfach mal eine
Seite über unser Projekt mit `hugo new about.md`. Seiten werden in `content/` und Blogposts in `content/posts`
abgelegt. Zu Markdown gibts hier eine schöne [Übersicht](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
was man damit alles machen kann.
Posts legt man mit `hugo new posts/postname.md` an. Schreibt testweise einfach mal ein paar
Sätze in die eben angelegte Seite `about.md` rein.

Jetzt lasst uns auch mal sehen wie das ganze
aussieht: `hugo server -D` startet einen kleinen Webserver auf den wir zugreifen können. Das `-D`
bedeutet, dass auch "Drafts", also Entwürfe angezeigt werden. Neu angelegte Posts oder auch Seiten
sind standardmäßig mit einem Tag `Drafts: true` versehen. Unter `http://localhost:1313/` solltet
ihr jetzt eure Seite betrachten können. Sollte zwar teilweise Inhalt angezeigt werden, aber
anscheinend das Theme nicht angewendet werden, dann passt in der config.toml die `baseURL` an.
Für den lokalen betrieb sollte dort auch `localhost:1313` stehen. Sobald ihr die Seite auf einen
Server ausliefert, muss dort natürlich eure Domain rein.

## Aufbau von Hugo

Vielleicht ist es euch schon aufgefallen, als ihr die config.toml datei aus dem Theme
kopiert habt, dass die Ordnerstruktur im Projektordner eurer Seite der des Themes sehr
ähnelt. Das hat auch einen Grund: Im groben ist die Funktionsweise so, dass Hugo erst mal im
Wurzelverzeichnis eurer Seite nach den Dateien schaut und dann erst im Theme.

Das macht es vor allem einfach, das Theme welches ihr ausgewählt habt anzupassen. Nehmen wir
mal an, dass es eine CSS-Datei mit vordefinierten Farben im Verzeichnis eures Themes gibt, in der
Regel liegen diese unter `Theme/assets/css`. Nun kopiert ihr diese Datei nach
`/Seite/assets/css`. Anpassungen an den Farben in unserem Szenario werden also an der Datei im
Wurzelverzeichnis gemacht und so verhält es sich auch mit anderen Anpassungen am Theme, sei es das
Layout, Skripte oder andere Funktionen. Falls ihr nämlich mal euer Theme aktualisiert, werden
eure Änderungen nicht überschrieben bzw. müsst ihr diese mit git nicht erst mergen.

## Ich bin fertig, was jetzt?

Content steht bereit, Theme ist angepasst, dann können wir ja Ernst machen!
Mit einem `hugo -s ./` generiert uns nun unsere Seite in einen Ordner `public` oder
mit `-d Ziel` einen Ordner `Ziel`. Nun müsst ihr nur noch den Inhalt auf euren Webserver
schieben und Fertig. Wie ihr seht, ist der größte Aufwand die Einrichtung und
das anpassen an die eigenen Vorlieben. Es gibt noch viele weitere Funktionen von
Hugo, am besten schaut ihr da mal in die Dokumentation die ich schon Anfangs verlinkt habe rein.

Das war es erst mal mit einer kleinen Einführung in Hugo. Viel Spass beim ausprobieren!
