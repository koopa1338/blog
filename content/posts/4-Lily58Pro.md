---
title: "DIY Tastatur Lily58Pro"
date: 2021-09-19T19:32:58+02:00
draft: false
toc: false
featuredImg: /media/keyboard.jpg
tags: 
  - Hardware
  - Tastatur
  - Mechanisch
  - DIY
---

## Vorgeschichte

Aktuell bin ich dabei mir eine eigene mechanische Tastatur zusammen zu bauen.
Die Planung dafür hat aber schon in der Vergangenheit viel Zeit in Anspruch
genommen, die ich hier auch noch mal kurz aufgreifen möchte. Ich mag kleine
Tastaturen, weshalb mein ursprünglicher Plan war, mir ein sogenanntes 60%
Keyboard zu bauen. Bei dieser Bauweise fehlt der Nummernblock, sowie die
F-Tasten und Esc. Diese Tasten werden dann mittels der Firmware über Ebenen
realisiert, hierzu später mehr.

Ich hatte mich dann weiter mit Tastaturen in allen Farben und Formen sowie
Layouts beschäftigt und stellte schnell dabei fest, was mich an einer normalen
Tastatur stört:
1. Die Hände sind zu dicht beieinander wodurch die Handgelenke immer in einer
   angespannten Haltung sind.
2. Tastaturen sind aufgrund ihrer Historie nicht ortholinear

Den ersten Punkt würde man im Alltag vermutlich nicht bemerken, weil man immer
mal wieder die Hand von der Tastatur nimmt und die Maus bedient. Ich nutze
jedoch einen Tiling Window Manager, wo ich fast ausschließlich mit der Tastatur
arbeite, sei es Programme öffnen, Desktops wechseln, Herunterfahren/Neustarten
oder Fenster neu anordnen. Meine Maus nutzer ich dabei so gut wie gar nicht,
außer ggf. um im Browser auf etwas zu klicken (selbst hierfür gibt es Plugins,
dass man fast alles mit der Tastatur machen kann). Zum 2. Punkt hatte ich mal
eine ortholineare Tastatur getestet und das schreibgefühl war deutlich besser.
Man gewöhnt sich schnell daran und es ergibt auch viel mehr Sinn die Tasten
ortholinear anzuordnen.

So bin ich dann bei sehr minimalistischen Split-Keyboards gelandet. Diese
Tastaturen sind in der Mitte geteilt und kommunizieren über ein Kabel
miteinander. So lassen sich die Teile frei auf der Arbeitsfläche positionieren
und es passt dann auch noch der Kaffee dazwischen :wink:. Ich habe mich dann
letztenendes dafür entschieden eine Lily58Pro zu bauen. Die Tastatur ist Open
Source, sprich die Dateien für das herstellen einer Platine sind auf
[Github](https://github.com/kata0510/Lily58). Ich habe mir dann die Dateien
heruntergeladen und mittels KiCad die nötigen Gerber Dateien erstellt. Damit
konnte ich mir dann die nötigen Platinen auf JLCPCB fertigen und zuschicken
lassen. Für 10 Platinen (Also Platinen für 5 komplette Tastaturen) hat das ca
25€ gekostet. Alle weiteren Teile stehen in der Dokumentation und ein Gehäuse
lässt sich einfach 3D drucken.

## Firmware

Ein weiterer Vorteil ist die QMK Firmware. Mit dieser lässt sich die Tastatur
auf Firmwareebene in jeder erdenklichen Weise konfigurieren. Hierzu kann man
den [QMK Configurator](https://config.qmk.fm/) verwenden. Hier kommmen wir nun
auch zu den am Anfang beschriebenen Ebenen. Mittels Layertasten lassen sich
diese Ebenen Umschalten. Möglich sind bis zu 13 Ebenen, was selbst bei einem 58
Tasten Keyboard wirklich viele belegbare Tasten ergibt. Aber Ebenen lassen sich
nicht nur durch spezielle Tasten umschalten. Es gibt auch Optionen, dass wenn
man eine Taste nur kurz drückt ein Keycode gesendet wird, wenn man diese jedoch
gedrückt hält auf eine andere Ebene umgeschaltet wird. Die Möglichkeiten die
sich daraus ergeben sind nahezu grenzenlos, jedoch sollte man beachten, dass
die Tastatur nur Keycodes sendet, die Interpretation vom Betriebssystem ist vom
verwendeten Tastaturlayout abhängig. Ich habe mich dennoch dafür entschieden,
mit dem US International Layout zu arbeiten und Umlaute wie `ä`, `ö`, `ü` sowie
`ß` auf einer Ebene auszulagern. Das gibt mir vor allem beim Programmieren den
Vorteil Klammern deutlich angenehmer tippen zu können. Voraussetzung ist, dass
bei jedem Rechner wo ich diese Tastatur anschließen möchte das US Internatioal
Layout verfügbar und ausgewählt ist. Die Tasten sind extra darauf
zugeschnitten, dass neben dem QWERTZ Layout die am meisten benötigten Tasten
beim programmieren am besten erreichbar sind. Im Standard DE Layout ergibt
diese Konfiguration nicht viel Sinn.

## Aktueller Stand

Zum aktuellen Zeitpunkt habe ich alle Komponenten auf die Platinen gelötet. Mir
Fehlen nun noch die Switches, hier habe ich auch wieder die Qual der Wahl, da
durch das ausgelaufene Patent von Cherry nun haufenweise Hersteller mit neuen
Switches auf den Markt stürmen. Hier werde ich mich noch ein mal ausgiebig mit
beschäftigen, habe mir aber schon ein paar Favoriten ausgesucht die in Frage
kommen. Ich habe bei meinen Platinen sogenannte Hot Swap Halterungen für die
Switches aufgelötet. So kann ich Switches schnell tauschen und muss diese nicht
erst auslöten.

Ich werde hierzu noch einen weiteren Blogartikel schreiben, der sich mit der
fertigen Tastatur und einen ersten Erfahrungsbericht im Alltag beschäftigt und
auch einige Bilder beinhalten wird.
