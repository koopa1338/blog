---
title: "Kontaktformular mit Lua"
date: 2021-04-23T15:50:56+02:00
draft: false
toc: false
featuredImg: /media/servers.jpg
tags: 
  - Lua
  - Nginx
  - Infrastruktur
---

## Szenario

Wenn man schon mal eine kleine HTML-Seite gebaut hat, dann stolpert man
ziemlich schnell auch über HTML Formulare. Formulare sind eine Schnittstelle
zum Backend, sprich ein Fall in dem das Backend Benutzereingaben verarbeiten
muss. Der Ablauf dabei ist relativ simpel: 

- Formular wird mit einem POST Request an das Backend geschickt
- Webserver verarbeitet Parameter, in den meisten Fällen mit einem PHP-Skript

Ich möchte in diesem Beitrag eine Alternative vorstellen und die Verarbeitung
nicht mit PHP sondern mit Lua machen.

## Voraussetzung und Vorbereitungen

In diesem Beispiel wird als Webserver Nginx verwendet, ob auch andere Webserver
wie Apache ein Modul für Lua haben, habe ich nicht recherchiert. Zu erst müssen
wir ein paar Pakete installieren. Ich nutze hier ein Debian Buster, diese
Pakete sollten aber unter den meisten Distributionen vorhanden sein:

```sh
apt install nginx-extras lua5.1 luarocks postfix mailutils -y
```

Mit luarocks können wir zusätzliche lua Bibliotheken installieren. Wir werden
hier zum verarbeiten der Parameter `cjson` verwenden:

```sh
luarocks install cjson
```

## Nginx

Eine komplette Konfiguration des Nginx möchte ich an dieser stelle nicht durchkauen, stattdessen brauchen wir für unser Kontaktformular nur folgendes eintragen:

```
location /contact {
    if ($request_method != POST) {
        return 405;
    }
    content_by_lua_file /etc/nginx/lua/contact.lua;
}
```

Den Pfad und die Datei `/etc/nginx/lua/contact.lua` können wir jetzt schon mal
erstellen, zum Inhalt später mehr.

## Frontend Formular

Damit wir im Backend den Request als JSON verarbeiten können, müssen wir im Frontend unsere Parameter auch als JSON senden.

Hier Beispielhaft das HTML:

```html
<section id="contact">
    <form action="/contact" method="post" id="contact-form">
        <div class="field half first">
            <label for="name">Name</label>
            <input type="text" name="name" id="name" />
        </div>
        <div class="field half">
            <label for="email">Email</label>
            <input type="email" name="email" id="email" />
        </div>
        <div class="field">
            <label for="message">Nachricht</label>
            <textarea name="message" id="message" rows="6"></textarea>
        </div>
        <button type="submit" value="Send" class="special" />Senden</button>
    </form>
</section>
```

Wichtig ist hier die `id` des Formulars, da wir mittels Javascript und jQuery
nun das Formular als JSON absenden:

```js
(function($) {
    contactForm = $('#contact-form');

    contactForm.on('submit', function(event) {
        event.preventDefault();

        const form = event.currentTarget;
        const url = form.action;

        var name = $(this).find('input[name="name"]').val();
        var email = $(this).find('input[name="email"]').val();
        var message = $(this).find('textarea[name="message"]').val();

        var postDataRaw = {
            "name": name,
            "email": email,
            "message": message
        };

        var postData = JSON.stringify(postDataRaw);

        $.ajax({
            url: url, 
            type : "POST", 
            contentType: "application/json; charset=utf-8",
            data : postData,
            success: function() {
                contactForm[0].reset();
                console.log("SUCCESS");
            },
            error: function(error, msg) {
                console.log(error, msg);
            }
        });
    });
})(jQuery);
```

Jetzt haben wir das Frontend vorbereitet und senden unsere Formulardaten als
JSON an den Nginx, dort haben wir den Endpunkt definiert und rufen dort im
Kontext das lua Skript `contact.lua` auf.

## Lua Skript

In unserem Lua Skript müssen wir nun den Request verarbeiten und die Daten an
eine Emailadresse weiterleiten. Unser `contact.lua` sieht dann wie folgt aus:

```lua
local json = require('cjson')

local receipt = "<Empfänger>"

local name = ""
local email = ""
local message = ""

-- Get form data
ngx.req.read_body()
local args, err = ngx.req.get_post_args()

if not args then
    ngx.say("failed to get post args: ", err)
    return
end

-- Decode json
for json_string, _ in pairs(args) do
    local tab = json.decode(json_string)
    name = tab["name"]
    email = tab["email"]
    message = tab["message"]
end

-- Prepare mail body
local subject = string.format("Kontakt von %s (%s)", name, email)

-- sending email
mail = io.popen(string.format("mail -s '%s' %s", subject, receipt), "w")
mail:write(message .. "\n\4")
mail:close()

ngx.status = 200
ngx.say("Email erfolgreich versendet.")
return
```

Wir importieren das Paket `cjson`, das wir installiert haben. Mit `ngx.req`
können wir auf den Request zugreifen. Anschließend prüfen wir die Parameter und
setzen die entsprechenden Variablen, die wir zu Beginn des Skriptes erstellt
haben.

Im letzten Teil rufen wir den `mail` befehl auf, um die Email zu versenden. Wenn
alles geklappt hat, setzen wir den Statuscode 200.
