---
title: "Initial Post"
date: 2019-02-22T01:19:13+01:00
draft: false
toc: false
featuredImg: /media/Aenami-city.jpg
tags: 
---

## Es lebt!

{{< figres src="/media/Aenami-city.jpg" title="test" >}}
Wie schön, dass der Blog endlich nach meinem Geschmack funktioniert. 

Aufgesetzt habe ich das ganze mit Hugo, einem so genannte _static stite generator_.
Beiträge mit Mardown zu schreiben ist ziemlich angenehm und macht es mir auch möglich 
mit vim meine Beiträge zu schreiben. (Ein Hoch auf das Terminal!) Weiterer Vorteil: 

Ich habe kein aufgeblasenes CMS mit einer Datenbank für eine Seite, die eigentlich nur Text und 
Bilder bereitstellt. Das ist auch für euch von Vorteil, da so eine statische Seite um einiges performanter ist, 
keine Daten mit Google Analytics von euch getrackt werden und mein Blog dadurch ein richtiges Leichtgewicht ist.
Außerdem muss ich nicht diesen ganzen Updatewahn von Content Management Systemen mit machen. 

Letztenendes biete ich potentiellen Angreifern viel weniger Angriffsfläche und ich muss keine Angst haben wenn mal über eine längere 
Zeit nichts auf meinem Blog kommt oder ich mich aus zeitlichen Gründen nicht so intensiv darum kümmern kann.

Ich werde vielleicht als nächstes einen Beitrag dazu schreiben, wie man sowas genau aufsetzt, welche 
Schwierigkeiten man dabei haben kann und wie mein Workflow aussieht. Bis dahin, seid gespannt, denn Ideen 
für Projekte habe ich genug und es ist einiges in der Pipeline was bald kommen wird :smile:.
